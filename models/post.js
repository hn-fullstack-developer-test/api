let mongoose = require('mongoose')


let PostSchema = new mongoose.Schema({
    created_at: Date,
    title: String,
    autor: String,
    url: String
})

module.exports = mongoose.model('Post', PostSchema)