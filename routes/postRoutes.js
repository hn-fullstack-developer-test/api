const express = require("express");
const postModel = require("../models/post");
const app = express();
const mongoose = require("mongoose");

// db credentials
const user = "testhn";
const password = "pXpKKC1qPNlDrqZG";

var uri =
  `mongodb://${user}:${password}@` +
  "cluster0-shard-00-00-gvjef.mongodb.net:27017," +
  "cluster0-shard-00-01-gvjef.mongodb.net:27017," +
  "cluster0-shard-00-02-gvjef.mongodb.net:27017/hn?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";


// ! Get Method
app.get("/posts", async (req, res) => {
  mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
  });

  try {
    const posts = await postModel.find({});
    res.send(posts);
  } catch (err) {
    res.status(500).send(err);
  }
  mongoose.connection.close();
});

// ! Delete Method
app.delete("/post/:id", async (req, res) => {
  console.log("Deleting....");
  mongoose
    .connect(uri, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false
    })
    .then(() => console.log("db connected"));

  try {
    const post = await postModel.findByIdAndDelete(req.params.id);
    if (!post) res.status(404).send("No item found");
    res.status(200).send();
  } catch (err) {
    res.status(500).send(err);
  }
  mongoose.connection.close().then(() => console.log("db disconnected"));
});

// ! Post Method
app.post("/post", async (req, res) => {
  mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
  });
  try {
    const post = new postModel(req.body);
    await post.save();
    res.send(post);
  } catch (err) {
    res.status(500).send(err);
  }
  mongoose.connection.close();
});

// ! Patch Method
app.patch("/post/:id", async (req, res) => {
  mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
  });

  try {
    const post = await postModel.findByIdAndUpdate(req.params.id, req.body);
    await postModel.save();
    res.send(post);
  } catch (err) {
    res.status(500).send(err);
  }
  mongoose.connection.close();
});

module.exports = app;
