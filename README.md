# Server component: Node.js + Express + MongoDB(Cloud) 

This project contains the API and the Fetch functionality of the HN Project (Fullstack Developer Test).

## CI/CD

It is built with CI/CD functionalities through the .gitlad-ci.yml file located at the root of the project.

## Running local unit tests

To run local tests you must clone the project, run an `npm install`, and then an `npm test`.

## Run Local Server

To raise the API/Fetch run an `npm start`.

## Run Local Server via Docker

A Dockerfile file was also added, to run the functionality you must have Docker installed on your system, clone the project, cd to it and run the following command to create the image:

`docker image build -t fdt_api .`

And then the following to start it:

`docker container run --publish 3000:3000 --detach --name api fdt_api`

To check its operation visit the *http://localhost:3000* url in the browser.





## Additional Information

The Fetch functionality is configured, upon request, to run every hour (at minute 0), so to be tested in an integral way it is recommended to let it run for a reasonable time.

The DB functionality was implemented in the official MongoBD cloud (https://cloud.mongodb.com/), which took advantage of adding value to the development experience, because it now has a persistent data layer, suitable for taking out more useful to the test. The configuration detail and the connection to the DB can be seen in the "API\fetch\load.js" and "API\routes\postRoutes.js" files

There are currently enough records in the DB to adequately assess the integral operation of the requirement.


Finally, special care was taken to generate the specific cases to evaluate the operation of the API, it can be checked in the file "API\test\test.js" 




